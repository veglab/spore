SPORE
=====

# Présentation
Pile applicative Docker pour la mise à disposition de référentiels pour la flore, la phytosociologie et les habitats.

En quelques mots, l'application vous permet d'importer des référentiels au format CSV dans des index ("indices")
Elasticsearch et de les exposer. Plusieurs scripts permettent l'import (`ingest`) et la sauvegarde (`snapshot`) de
données ainsi que la gestion d'alias (`alias`).

Applications faisant partie de cette pile :
- Elasticsearch (ES)
- Logstash (LS)
- Traefik (TF)

Les référentiels sont stockés dans des index Elasticsearch et diffusés au travers d'urls du type `https://(www.)sous-domaine.mon-domaine.com/mon-referentiel`.

L'interrogation des différents référentiels se fait via des [requêtes Elastisearch classiques](https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl.html).

L'accès à l'application se fait un travers d'utilisateurs anonymes pour la consultation de données et nécessite
un compte administrateur pour la création d'index ("référentiels", "repositories") ou d'alias ainsi que pour l'écriture de données.

Un filtre par adresses IP limite les accès à l'application.

La mise à jour des référentiels se fait via Logstash. Les scripts `ingest` et `alias` vous aident dans cette tâche.

# Installation
- Prérequis
    - L'application est testée sous Ubuntu 20.10 et nécessite, notamment, la librairie Curl.
    - [Docker](https://docs.docker.com/get-docker/) ≥ 20.10
- Cloner le dépôt : `git clone XXX /destination`
- Dupliquer le fichier `/config/.env.sample` vers `/config/.env` et compléter les paramètres (voir la section suivante)
- Créer les dossiers `logs/elasticsearch`, `data/elasticsearch` et `snapshots` à la base du projet. Attribuez les droits 777 sur ces dossiers (au premier lancement, Elasticsearch affecte les droits nécessaires plus restreints)
- Lancer le projet : `./up -d`
- Créer un compte administrateur : `docker exec -it spore_elastic bash -c "bin/elasticsearch-users useradd **admin** -p **password123** -r role_admin"`
- Arrêter le projet pour l'instant : `./down`

# Configuration
Dupliquer le fichier `/config/.env.sample` vers `/config/.env` et compléter les paramètres :
- `CLUSTER_NAME` : nom du [cluster Elasticsearch](https://www.elastic.co/guide/en/elasticsearch/reference/current/add-elasticsearch-nodes.html).
- `APP_SUBDOMAIN` : nom de sous-domaine de l'application. Laisser vide si nécessaire. Ne pas entrer `www` comme nom de sous-domaine
  , celui-ci est géré automatiquement.
- `APP_HOST` : nom de domaine de l'application.
- `TRAEFIK_USER` : nom d'utilisateur pour se connecter à l'interface de Treafik.
- `TRAEFIK_PASSWORD` : mot de passe Traefik. Un nouveau mot de passe peut être généré avec la commande
  `echo $(htpasswd -nB **mon-nom-d-utilisateur**)`, les caractères spéciaux (notamment `$`) doivent être échappés (` \ `).
- `ELK_VERSION` : numéro de version de la stack Elasticsearch-Logstash-Kibana (ELK) utilisée. (note : Kibana n'est pas utilisé ici).
- `ELASTIC_PASSWORD`: mot de passe par défaut pour Elasticsearch.
- `ELASTIC_IP_WHITELIST` : liste des adresses IP autorisées à se connecter à l'application Elasticsearch.
- `ADMIN_EMAIL` : adresse e-mail utilisée pour la génération du certificat SSL.
- `ADMIN_USER` : nom de l'utilisateur du compte administrateur. (utilisé uniquement pour les tests)
- `ADMIN_PASSWORD` : mot de passe du compte administrateur. (utilisé uniquement pour les tests)

Pour un développement en local, ajouter les hôtes à votre fichier `/etc/hosts`.

Exemple avec `APP_SUBDOMAIN=local` et `APP_HOST=spore.io` :
```
127.0.0.1 local.spore.io
127.0.0.1 local.traefik.spore.io
```

Plusieurs fichiers ou dossiers de l'hôte sont montés dans le conteneur ES lors du démarrage de celui-ci :
- `/config/elasticsearch/elasticsearch.yml` : fichier de configuration de l'application ES.
- `/config/elasticsearch/roles.yml` : configuration des rôles.
- `/data/elastisearch`: les données.
- `/logs/elasticsearch` : les logs bruts.

Un volume est créé pour conserver la configuration ES (notamment les utilisateurs) : `config`. Sur l'hôte, il est accessible
dans `/var/lib/docker/volumes/spore_config/`. La sauvegarde des utilisateurs (transfert...) pourra
être réalisée en sauvegardant le fichier `users` de ce volume.

Le service Elasticsearch est restreint à une liste d'adresse IP définie dans le fichier `.env`. Sur le serveur de 
production, ne pas oublier d'indiquer l'adresse IP du serveur lui-même.  
Afin de permettre à LetEncrypt de générer automatiquement les certificats, il est nécessaire de désactiver, manuellement, 
le filtre par adresse IP. Pour ce faire, commenter la ligne `# apply (use) middleware` du fichier docker-compose.yml.  

Un certificat SSL est automatiquement créé et mise à jour par Traefik. En local, un avertissement de sécurité peut
apparaitre dans votre navigateur.

# Utilisation (dev)
## Architecture des dossiers
- `/config`
    - `.env` : configuration de l'application.
    - `/elasticsearch` : paramètres de l'application ES (général, rôles, sécurité...).
    - `/logstash` : paramètres de l'application Logstash.
- `/docker` : contient les Dockerfiles ES + LS.
- `/data` : stockage des données ES.
- `/snapshots` : stockage des sauvegardes ES.
- `/dumps` : stockage des dumps ES.
- `/logs` : stockage des logs ES + LS.
- `/repositories-pipeline` : fichiers sources et configurations des différents référentiels.

## Commandes disponibles
- `./up` : mise en route du service.
    - `-d` : en mode détaché.
    - `--build` : build et mise en route du service.
- `./down` : arrête le service.
- `./ingest` : intègre un référentiel (ficher CSV) dans un index.
- `./drop` : supprimer un index.
- `./list-indices` : renvoie la liste de tous les index et de tous les alias.
- `./alias` : créer ou supprimer un alias.
- `./snapshot` : créer une sauvegarde du cluster.
- `./tests/run-test` : lance les tests automatisés.

## Sauvegardes
### Snapshots
Les sauvegardes des index Elasticsearch se font au travers de [snapshots](https://www.elastic.co/guide/en/elasticsearch/reference/current/snapshot-restore.html).  
Ces snapshots sont utiles pour restaurer une version antérieure du cluster (= l'ensemble des index) mais ne sont **pas exportables**.

Les snapshots sont entièrement gérés par Elasticsearch et ne sont pas exportables. Ils se trouvent dans le dossier `snapshot`.

Liste des snapshots :

```bash
./snapshot list
```

Créer un nouveau snapshot :
```bash
./snapshot create
```

Restaurer à partir d'un snaphot :
```bash
./snapshot restore
```

Supprimer un snapshot :  
@Todo

### Dumps
@Todo Ajouter la possibilité de faire des dumps : https://github.com/elasticsearch-dump/elasticsearch-dump


## Gestion des utilisateurs
### Rôles
Deux rôles sont proposés par défaut dans l'application :
- `role_user` : restreint l'accès à l'ensemble des données (index, documents) en lecture seule.
- `role_admin ` : donne un accès total sur le cluster, ses index et les documents.

Elasticsearch propose [deux modes de gestion des rôles](https://www.elastic.co/guide/en/elasticsearch/reference/current/defining-roles.html)
par défaut. Une méthode par API, très souple et dynamique et une méthode par fichier, plus statique. C'est cette dernière qui
est utilisée dans l'application.

### Ajouter ou modifier un rôle
Les rôles sont définis dans `/config/elasticsearch/roles.yml`. Ils peuvent être modifiés à chaud dans le fichier
`/config/elasticsearch/roles.yml` sans qu'un redémarrage des serveurs ne soit nécessaire pour leur prise en compte.
Voir la [documentation ES à ce sujet](https://www.elastic.co/guide/en/elasticsearch/reference/current/defining-roles.html).

### Utilisateur
Tous les utilisateurs anonymes bénéficient du rôle `role_user` par défaut.
Cela signifie qu'il n'est pas nécessaire de fournir une authentification pour lire les données.

### Ajouter ou modifier un utilisateur
Il est nécessaire de créer au moins un utilisateur `admin` afin de peupler et de gérer les référentiels (requêtes PUT, POST, DELETE).

Elasticsearch fournit un outil en ligne de commande `elasticsearch-users` pour gérer les utilisateurs. [Voir la documentation ES](https://www.elastic.co/guide/en/elasticsearch/reference/current/users-command.html).

Exemples :
- Ajouter un utilisateur (admin) : `docker exec -it spore_elastic bash -c "bin/elasticsearch-users useradd admin -p secretpassword -r role_admin,logstash_admin"`
- Supprimer un utilisateur : `docker exec -it spore_elastic bash -c "bin/elasticsearch-users userdel username"`
- Liste des utilisateurs : `docker exec -it spore_elastic bash -c "bin/elasticsearch-users list"`


## Intégrer un référentiel
Logstash est utilisé pour intégrer des données dans les index Elasticsearch.

Le script `ingest` permet de faciliter le processus d'intégration.

```bash
./ingest repo/taxref-15.csv
```

En arrière-plan, le fichier CSV est lu afin de créer un fichier de configuration pour l'import.  
Un conteneur Logstash est lancé et utilise le fichier de configuration pour intégrer votre fichier CSV.

Lors de l'import avec `ingest`, vous devez vérifier le séparateur de champs, le nombre de données
d'en-tête (colonnes) ainsi que le nombre de données intégrées.

## Supprimer un référentiel
Voir la [documentation officielle](https://www.elastic.co/guide/en/elasticsearch/reference/current/indices-delete-index.html)

Le script `drop` permet de faciliter la suppression d'un index.

```bash
./drop my-index
```

Si un alias existe pour cet index, il sera également supprimé.

## À propos des alias
Voir la [documentation officielle](https://www.elastic.co/guide/en/elasticsearch/reference/current/aliases.html) pour comprendre le fonctionnement des alias.

Afin de faciliter la gestion d'un même référentiel dans plusieurs versions, vous pouvez utiliser des alias renvoyant vers
l'une ou l'autre de ces versions que vous aurez préalablement placées dans des index différents.

Exemple :
- l'index "taxref-14" contient le référentiel TAXREF en version 14
- l'index "taxref-15" contient la version 15
- l'alias "taxref" peut pointer vers l'un ou l'autre de ces index

Il est ainsi plus facile d'utiliser l'alias "taxref" et de mettre celui-ci à jour au besoin.

Exemple de mise à jour de version pour un référentiel :
```bash
$ ./alias drop taxref taxref-14 && ./alias add taxref taxref-15
You want to REMOVE the alias "taxref" on the index "taxref-14". Is this correct ? (y/N) y
The alias "taxref" has been removed on index "taxref-14".
You want to create an alias "taxref" for the index "taxref-15". Is this correct ? (y/N) y
Alias "taxref" has been created on index "taxref-15".
```

_Note : Elasticsearch autorise l'utilisation d'un même alias pointant vers différents index. Ce type de fonctionnement ne convient
pas dans notre cas d'usage et le script `alias` ne permet pas cela._

## Tests
Quelques tests automatisés permettent de vérifier le bon fonctionnement de l'application. Il s'agit de simples requêtes Curl
permettant de vérifier la connexion à l'instance ES ainsi que les droits des utilisateurs.

Avant de lancer les tests, il est nécessaire de créer un utilisateur avec le rôle `role_admin` et de renseigner ses identifiants
dans le fichier `.env`. Le login et le mot de passe renseignés dans ce fichier ne sont utilisés que pour les tests.

Pour lancer les tests :
```bash
./tests/run-tests
```  

_Note : un index `repo_test`est créé puis supprimé durant les tests._

# Utilisation (user)
### Liste des référentiels disponibles
### Accéder à un référentiel
### Requêter un référentiel
